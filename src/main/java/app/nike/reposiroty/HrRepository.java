package app.nike.reposiroty;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import app.nike.entity.Hr;

@Repository
public interface HrRepository extends MongoRepository<Hr, String> {
	
	public Hr findByUsername(String username);

	public Hr findByEmail(String email);
}
