package app.nike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Id10CrudHrApplication {

	public static void main(String[] args) {
		SpringApplication.run(Id10CrudHrApplication.class, args);
	}

}
