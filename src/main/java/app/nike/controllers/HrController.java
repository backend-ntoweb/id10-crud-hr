package app.nike.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.nike.entity.Hr;
import app.nike.service.HrService;

@RestController
@RequestMapping("/hr")
public class HrController {

	@Autowired
	private HrService hrService;

	@PostMapping("/creaHr")
	public Hr creaHr(@RequestBody Hr hr) throws Exception {
		return hrService.creaHr(hr);
	}

	@GetMapping("/{username}/{email}")
	public Hr getByusernameOrEmail(@PathVariable(value = "username") String username,
			@PathVariable(value = "email") String email) {
		try {
			return hrService.getByUsernameOrEmail(username, email);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@GetMapping("/{username}")
	public Hr getByUsername(@PathVariable(value = "username") String username) {
		try {
			return hrService.getByUsername(username);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@DeleteMapping("/cancella/{username}")
	public void deleteByUsername(@PathVariable(value = "username") String username) {
		try {
			hrService.deleteByUsername(username);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@GetMapping("/leggi")
	public List<Hr> findAll() {
		try {
			return hrService.findAll();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	@PutMapping("/update")
	public Hr updateHr(@RequestBody Hr hr) throws Exception {
		return hrService.updateHr(hr);
	}
}
