package app.nike.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.nike.entity.Hr;
import app.nike.reposiroty.HrRepository;

@Service
public class HrServiceImpl implements HrService {
	@Autowired
	private HrRepository hrRepository;

	@Override
	public Hr creaHr(Hr hr) throws Exception {
		if(hr==null)throw new Exception("Non è presente nessun hr da salvare");
		try {
			if(hrRepository.findByUsername(hr.getUsername())!=null)throw new Exception("Username già presente nel db");
			return hrRepository.save(hr);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("Il salvataggio non è andato a buon fine");
		}
	}

	@Override
	public Hr getByUsernameOrEmail(String username, String email) throws Exception {
		try {
			Hr hrToReturn = hrRepository.findByEmail(email);
			if (hrToReturn == null) {
				return hrRepository.findByUsername(username);
			} else
				return null;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("La ricerca tramite la email o lo username non è andata a buon fine");
		}
	}

	@Override
	public Hr getByUsername(String username) throws Exception {
		try {
			return hrRepository.findByUsername(username);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("la ricerca tramite uno username non è andata a buon fine");
		}
	}

	@Override
	public void deleteByUsername(String username) throws Exception {
		Hr hrToDelete;
		try {
			hrToDelete = hrRepository.findByUsername(username);
			if (hrToDelete == null) {
				throw new NullPointerException();
			} else {
				hrRepository.delete(hrToDelete);
			}
		} catch (Exception e) {
			throw new Exception("La delete non è andata a buon fine");
		}
	}

	@Override
	public List<Hr> findAll() throws Exception {
		try {
			return hrRepository.findAll();
		} catch (Exception e) {
			throw new Exception("findAll non è andata a buon fine");
		}
	}
	
	@Override
	public Hr updateHr(Hr hr) throws Exception {
		if(hr==null) throw new Exception("Hr da modificare vuoto");
		
		Hr hrToUpdate;
		try {
			hrToUpdate= hrRepository.findByUsername(hr.getUsername());
			
			if(hrToUpdate==null) throw new Exception("Hr non presente nel Db");
			
			hrToUpdate.setNome(hr.getNome());
			hrToUpdate.setCognome(hr.getCognome());
			hrToUpdate.setEmail(hr.getEmail());
			
			System.out.println(hrToUpdate.getId().toString());
			
			return hrRepository.save(hrToUpdate);
		}catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("L'update non è andato a buon fine");
		}
	}
}
