package app.nike.service;

import java.util.List;

import app.nike.entity.Hr;

public interface HrService {

	public Hr creaHr(Hr hr) throws Exception;

	public Hr getByUsernameOrEmail(String username, String email) throws Exception;

	public Hr getByUsername(String username) throws Exception;

	public void deleteByUsername(String username) throws Exception;

	public List<Hr> findAll() throws Exception;

	public Hr updateHr(Hr hr) throws Exception;

}
